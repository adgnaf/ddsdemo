#include "RobotCtrlNode.h"
#include <fastrtps/Domain.h>
#include <fastrtps/utils/eClock.h>
#include <fastrtps/log/Log.h>

using namespace eprosima;
using namespace fastrtps;
using namespace rtps; 

int main(int argc, char** argv) {
    std::cout << "Starting" << std::endl;
    RobotCtrlNode sub;
    if (sub.init()) {
        sub.run();
    }
    Domain::stopAll();
    Log::Reset();
    return 0;
}