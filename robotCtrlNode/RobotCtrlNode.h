#ifndef ROBOTCTRLNODE_H_
#define ROBOTCTRLNODE_H_ 

#include "CommMsgPubSubTypes.h"
#include <fastrtps/fastrtps_fwd.h> 
#include <fastrtps/attributes/SubscriberAttributes.h>
#include <fastrtps/subscriber/SubscriberListener.h> 
#include <fastrtps/subscriber/SampleInfo.h> 

#include "CommMsg.h"

class RobotCtrlNode {

    public: 
        RobotCtrlNode();
        virtual ~RobotCtrlNode();
        bool init();
        void run();
        void run(uint32_t number);
    private:
        eprosima::fastrtps::Participant* mp_participant;
        eprosima::fastrtps::Subscriber* mp_subscriber;
    public:
        class SubListener:public eprosima::fastrtps::SubscriberListener
        {
        public:
            SubListener():n_matched(0), n_samples(0){};
            ~SubListener(){}; 
            void onSubscriptionMatched(eprosima::fastrtps::Subscriber* sub, 
                        eprosima::fastrtps::rtps::MatchingInfo& info);
            void onNewDataMessage(eprosima::fastrtps::Subscriber* sub);
            CommMsg m_CommMsg;
            eprosima::fastrtps::SampleInfo_t m_info;
            int n_matched;
            uint32_t n_samples;

        } m_listener;

    private:
        CommMsgPubSubType m_type;

};

#endif /* ROBOTCTRLNODE_H_*/