#include "DecisionMakingNode.h"
#include <fastrtps/participant/Participant.h>
#include <fastrtps/attributes/ParticipantAttributes.h>
#include <fastrtps/attributes/PublisherAttributes.h>
#include <fastrtps/publisher/Publisher.h>
#include <fastrtps/Domain.h>
#include <fastrtps/utils/eClock.h>

using namespace eprosima::fastrtps;
using namespace eprosima::fastrtps::rtps;

DecisionMakingNode::DecisionMakingNode():mp_participant(nullptr), mp_publisher(nullptr)
{

}

bool DecisionMakingNode::Init()
{
    m_CommMsg.index(0);
    m_CommMsg.message("RobotCtrlCmd");
    ParticipantAttributes PParam;
    PParam.rtps.defaultSendPort = 11511;
    PParam.rtps.use_IP4_to_send = true;
    PParam.rtps.builtin.use_SIMPLE_RTPSParticipantDiscoveryProtocol = true;
    PParam.rtps.builtin.use_SIMPLE_EndpointDiscoveryProtocol = true;
    PParam.rtps.builtin.m_simpleEDP.use_PublicationReaderANDSubscriptionWriter = true;
    PParam.rtps.builtin.m_simpleEDP.use_PublicationWriterANDSubscriptionReader = true;
    PParam.rtps.builtin.domainId = 0;
    PParam.rtps.builtin.leaseDuration = c_TimeInfinite;
    PParam.rtps.setName("Participant_pub");
    mp_participant = Domain::createParticipant(PParam);

    if(mp_participant == nullptr) {
        return false;
    }

    Domain::registerType(mp_participant, &m_type);

    PublisherAttributes Wparam;
    Wparam.topic.topicKind = NO_KEY;
    Wparam.topic.topicDataType = "CommMsg";
    Wparam.topic.topicName = "RobotCmdTopic";
    Wparam.topic.historyQos.kind  = KEEP_LAST_HISTORY_QOS;
    Wparam.topic.historyQos.depth = 30;
    Wparam.topic.resourceLimitsQos.max_samples = 50;
    Wparam.topic.resourceLimitsQos.allocated_samples = 20;
    Wparam.times.heartbeatPeriod.seconds = 2;
    Wparam.times.heartbeatPeriod.fraction = 200*1000*1000;
    Wparam.qos.m_reliability.kind = RELIABLE_RELIABILITY_QOS;
    mp_publisher = Domain::createPublisher(mp_participant
                                        , Wparam
                                        , (PublisherListener*)&m_listener);
    if (mp_publisher == nullptr) {
        return false;
    }
    return true;
}

DecisionMakingNode::~DecisionMakingNode() 
{
    Domain::removeParticipant(mp_participant);
}

void DecisionMakingNode::PubListener::onPublicationMatched(Publisher* pub, MatchingInfo& info) 
{
    if (info.status == MATCHED_MATCHING) 
    {
        n_matched++;
        std::cout << "Publisher matched" << std::endl;
    }
    else 
    {
        n_matched--;
        std::cout << "Publisher unmatched" << std::endl;
    }
}

void DecisionMakingNode::run(uint32_t samples) 
{
    for(uint32_t i = 0; i < samples; ++i) {
        if (!publish()) {
            --i;
        } else {
            std::cout << "Message: " << m_CommMsg.message() 
                      << " with index: " << m_CommMsg.index() 
                      << " SENT" << std::endl;
        }
        eClock::my_sleep(25);
    }
}

bool DecisionMakingNode::publish() 
{
    if (m_listener.n_matched > 0) {
        m_CommMsg.index(m_CommMsg.index() + 1);
        mp_publisher->write((void*)&m_CommMsg);
        return true;
    }
    return false;
}