#ifndef DECISIONMAKINGNODE_H_
#define DECISIONMAKINGNODE_H_

#include "CommMsgPubSubTypes.h"
#include <fastrtps/fastrtps_fwd.h> 
#include <fastrtps/attributes/PublisherAttributes.h> 
#include <fastrtps/publisher/PublisherListener.h>

#include "CommMsg.h"

class DecisionMakingNode {
    public:
        DecisionMakingNode();
        virtual ~DecisionMakingNode();
        bool Init();
        bool publish();
        void run(uint32_t number);

    private:
        CommMsg m_CommMsg;
        eprosima::fastrtps::Participant* mp_participant;
        eprosima::fastrtps::Publisher* mp_publisher; 
        class PubListener:public eprosima::fastrtps::PublisherListener
        {
        public:
            PubListener():n_matched(0){};
            ~PubListener(){};
            void onPublicationMatched(eprosima::fastrtps::Publisher* pub, 
                                    eprosima::fastrtps::rtps::MatchingInfo& info);
            int n_matched;
        }m_listener;
        CommMsgPubSubType m_type;
};

#endif /* DECISIONMAKINGNODE_H_ */